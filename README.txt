
-- SUMMARY --

Excel Generator Url provides an URL that forces the download of an excel file.
The content of the excel file is recorded on the $_SESSION array but alternatively you
can call the function directly passing the same structured array of data as parameter.

This last way of using the modules requires that you create a reception URL, but is 
preferable on a performance point of view when there are many users and the amount data 
is big as the $_SESSION is copied as a file by PHP. 

This module is interesting to allow fast coding as it gives a ready to use URL that you
can call from your theme or modules to download an excel file. 

For a full API access of PHPExcel you should use the Drupal PHPExcel module.
(see: https://www.drupal.org/project/phpexcel)

Available functionalities are: 
* Output formats Excel5, Excel2007 and CSV
* Coloring of cells and rows
* Single or multiple sheets

This module depends on the third-party API library PHPExcel.

For a full description of the module, visit the project page:
  http://drupal.org/project/excelgeneratorurl
  
To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/excelgeneratorurl
  
-- REQUIREMENT --

* PHPExcel v1.8.0 or higer needs to be downloaded on the libraires folder
  https://github.com/PHPOffice/PHPExcel

* PHPExcell requires PHP version 5.2.0 or higher 

   
-- INSTALLATION --

* Install excelgeneratorurl module as any other Drupal module.
  see:   http://drupal.org/documentation/install/modules-themes/modules-7
  
* Create a folder called phpexcel inside sites/all/libraries and paste the 
API can be downloaded at https://github.com/PHPOffice/PHPExcel

----------------
-- HOW TO USE --
----------------

There are two ways of using this module, storing the data on a $_SESSION variable 
and simply accessing the url /excelgeneratorurl/generate to print the last stored structure 

OR 

using a function and passing the setup parameters to it. 

----------------------------
| WAY 1 | $_SESSION variable
----------------------------

The module force the download of an excel file by calling the url 

/excelgeneratorurl/generate?eguformat=Excel2007

The format can be set on the url, valid formats are Excel5 or Excel2007, in case
of missing or erroneous format on the URL Excel2007 will be used.

To store multiple data arrays use $_SESSION['excelgeneratorurl'][ $index ]
You can then call the url requesting a certain index with the egldata param as follows:

/excelgeneratorurl/generate?eguformat=Excel2007&eguindex=1

If eguindex is not declared on the URL call then the index 0 will be used to fill 
the excel data.

The module will search the first data array stored in the 
$_SESSION['excelgeneratorurl'][0] variable that should be formated 
as following:

$_SESSION['excelgeneratorurl'][0] = array(
  'creator'=>'Excel Generator for Drupal',
  'modifiedby'=>'Excel Generator for Drupal',
  'title'=>'Excel Document Title',
  'subject'=>'Excel Document Subject',
  'description'=>'Excel Document Description',
  'keywords'=>'Excel Document Office',
  'category'=>'Excel Document Office',
  'filename'=>'excel-generated',
  'format'=>'Excel2007',
      
  // (optional) The sheet index that will be opened when Excel opens the document. Default 0
  'activesheet' => 0, 
  'sheets' => array(
    // the key is the title of the sheet. 
    // max length is 30 CHARS, will be trimmed.
    'Sheet title 1' => array(  
      array('header1','header2','header3','header4'),
      array('value1','value2','value3','value4'),
      array('value1','value2','value3','value4'),
      array('value1','value2','value3','value4'),
      array('value1','value2','value3','value4'),
      array('value1','value2','value3','value4'),
      array('value1','value2','value3','value4'),
      array('value1','value2','value3','value4'),
    ),
    'Sheet title 2' => array( 
      array('header1','header2','header3','header4'),
       array('value1','value2','value3','value4'),
      array('value1','value2','value3','value4'),
      array('value1','value2','value3','value4'),
      array('value1','value2','value3','value4'),
      array('value1','value2','value3','value4'),
      array('value1','value2','value3','value4'),
      array('value1','value2','value3','value4'),
    ),
    // Add as many sheets as you like
  ),
  'colours'=>array(
    // set the same key as the sheet title above to apply
    'Sheet title 1' => array( 
      array('cell'=>'A1:C1', 'colour'=>'EE2233'),
      array('cell'=>'C2', 'colour'=>'BBFF33'),
      array('cell'=>'D3:F3', 'colour'=>'6699DD'),
    ),
    'Sheet title 2' => array(
      array('cell'=>'A1:C1', 'colour'=>'EE2233'),
      array('cell'=>'C2', 'colour'=>'BBFF33'),
      array('cell'=>'D3:F3', 'colour'=>'6699DD'),
    ),						
  )
);

The first settings are used for the global parameters of the excel file,
the table it self is stored in the table array ordered in its sheet and colors can be set on the 
colors array also by ordering them in sheets. 
For the colours the 'cell' and 'color' key names need to be respected.

--------------------------------------------------------------------------------------------
| WAY 2 | Calling the function excelgeneratorurl_generation_page($excelgeneratorurl = FALSE)
--------------------------------------------------------------------------------------------

You can also call the function directly from within your own module/theme. 
Think about setting Excel Generator as dependency then (see: https://www.drupal.org/node/542202)

The exact same structure used previously on the $_SESSION should be passed as parameter
to the function for the module to work properly.

Notice that if you choose to use the function instead of the url already set and the 
$_SESSION you will need to create your self page and pass the params your self.

The function excelgeneratorurl_generation_page() will end up outputing an excel or csv file 
and for this to work not a single character should be outputted before or after the 
function is called. Read the next section: IMPORTANT NOTE IN CASE OF UNREADABLE EXCEL FILE.

-----------------------------------------------------
-- IMPORTANT NOTE IN CASE OF UNREADABLE EXCEL FILE -- 
-----------------------------------------------------

* NONE OF THE MODULES or THEMES HAS TO OUTPUT ANY EMPTY CHAR FOR EXCEL GENERATOR 
  TO WORK PROPERLY. IF YOU HAVE AN UNREADABLE EXCEL FILE, DISABLE MODULES ONE BY ONE
  AND TRY AGAIN UNTIL YOU FIND THE MODULE THAT HAS AN EMPTY SPACE SOMEWHERE.
  THIS CAN HAPPEN FOR INSTANCE BY HAVING A ?> <?php ON THE CODE. 
  
-- CONTACT --

Current maintainer:
* Sergio Garcia Fernandez (cubeinspire) - https://www.drupal.org/user/1039472